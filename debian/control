Source: netcdf
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Francesco Paolo Lovergine <frankie@debian.org>,
           Nico Schlömer <nico.schloemer@gmail.com>,
           Bas Couwenberg <sebastic@debian.org>
Section: science
Priority: optional
Build-Depends: cmake (>= 3.6.1),
               chrpath,
               debhelper-compat (= 13),
               doxygen,
               graphviz,
               libcurl4-gnutls-dev | libcurl-ssl-dev,
               libhdf5-dev,
               libxml2-dev,
               m4
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-gis-team/netcdf
Vcs-Git: https://salsa.debian.org/debian-gis-team/netcdf.git
Homepage: http://www.unidata.ucar.edu/software/netcdf/
Rules-Requires-Root: no

Package: netcdf-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Description: Documentation for NetCDF
 NetCDF (network Common Data Form) is an interface for scientific
 data access and a freely-distributed software library that provides an
 implementation of the interface.  The netCDF library also defines a
 machine-independent format for representing scientific data.
 Together, the interface, library, and format support the creation,
 access, and sharing of scientific data.
 .
 This package contains documentation for the NetCDF library in a
 variety of formats.

Package: libnetcdf22
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Interface for scientific data access to large binary data
 NetCDF (network Common Data Form) is an interface for scientific
 data access and a freely-distributed software library that provides an
 implementation of the interface.  The netCDF library also defines a
 machine-independent format for representing scientific data.
 Together, the interface, library, and format support the creation,
 access, and sharing of scientific data.
 .
 This package contains the C run-time shared libraries required
 by programs.

Package: libnetcdf-dev
Architecture: any
Section: libdevel
Depends: libnetcdf22 (= ${binary:Version}),
         libcurl4-gnutls-dev | libcurl-ssl-dev,
         libhdf5-dev,
         libxml2-dev,
         ${misc:Depends}
Recommends: pkgconf
Breaks: netcdf-bin (<< 1:4.3.3.1)
Replaces: netcdf-bin (<< 1:4.3.3.1)
Suggests: netcdf-bin,
          netcdf-doc
Description: creation, access, and sharing of scientific data
 NetCDF (network Common Data Form) is a set of interfaces for array-oriented
 data access and a freely distributed collection of data access libraries for
 C, Fortran, C++, Java, and other languages. The netCDF libraries support a
 machine-independent format for representing scientific data. Together, the
 interfaces, libraries, and format support the creation, access, and sharing of
 scientific data.
 .
 This package provides headers.

Package: netcdf-bin
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Programs for reading and writing NetCDF files
 Contains the programs ncdump and ncgen which convert NetCDF
 files to ASCII and back, respectively. NetCDF (network Common Data
 Form) is an interface for scientific data access and a
 freely-distributed software library that provides an implementation
 of the interface.  The netCDF library also defines a
 machine-independent format for representing scientific data.
 Together, the interface, library, and format support the creation,
 access, and sharing of scientific data.
